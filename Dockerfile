FROM node:12.2.0

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY package.json /app/package.json
RUN npm install
RUN npm install -g @angular/cli@7.3.9

COPY . /app

EXPOSE 4200

CMD ng serve --host=0.0.0.0
#Source
#https://mherman.org/blog/dockerizing-an-angular-app/
#https://christianlydemann.com/creating-a-mean-stack-application-with-docker/
#create Image:
#sudo docker build -t todoapp-frontend-image:dev .
#create Container:
#sudo docker run -d -v ${PWD}:/app -p 4200:4200 todoapp-frontend-image:dev
