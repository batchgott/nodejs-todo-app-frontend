import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {ItemService} from '../../services/item.service';
import {ActivatedRoute, Router} from '@angular/router';
import {take} from 'rxjs/operators';
import {Item} from '../../model/item.model';

@Component({
  selector: 'app-item-edit',
  templateUrl: './item-edit.component.html',
  styleUrls: ['./item-edit.component.scss']
})
export class ItemEditComponent implements OnInit {

  changeItemForm=new FormGroup({
    title:new FormControl('',[Validators.required]),
    description:new FormControl('',[Validators.required])
  });

  item:Item;

  constructor(private snackBar:MatSnackBar,
              private itemService:ItemService,
              private router:Router,
              private route:ActivatedRoute) { }

  ngOnInit() {
    this.route.params.pipe(take(1)).subscribe(params=>{
      this.itemService.getItemById(params['itemid']).pipe(take(1)).subscribe(item=>{
        this.item=item;
        this.changeItemForm=new FormGroup({
          title:new FormControl(item.title,[Validators.required]),
          description:new FormControl(item.description,[Validators.required])
        });
      })
    });
  }

  editItem() {
    this.item.title=this.changeItemForm.get("title").value;
    this.item.description=this.changeItemForm.get("description").value;
    this.itemService.updateItem(this.item).pipe(take(1)).subscribe((item:Item)=>{
      this.snackBar.open(`Das Todo ${item.title} wurde geändert`,null,{duration:3000});
      this.router.navigate(['/todos']);
    },err=>{
      this.snackBar.open("Es ist ein Fehler aufgetreten","Schließen",{duration:10000,panelClass:['warn-snackbar']});
    })
  }
}
