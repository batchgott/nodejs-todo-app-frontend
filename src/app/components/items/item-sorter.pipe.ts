import { Pipe, PipeTransform } from '@angular/core';
import {Item} from '../../model/item.model';

@Pipe({
  name: 'itemSorter',
  pure:false
})
export class ItemSorterPipe implements PipeTransform {

  transform(items: Item[], sorter: string): Item[] {
    if (!items||items.length===0 || sorter===null)
      return items;
    if (sorter === 'date')
      return items.sort((a, b) => a.date<b.date?1:a.date>b.date?-1:0);
    else
      return items.sort((a, b) =>(a.done===b.done)? 0:a.done? -1:1);
  }

}
