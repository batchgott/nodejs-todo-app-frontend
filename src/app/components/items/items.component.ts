import { Component, OnInit } from '@angular/core';
import {ItemService} from '../../services/item.service';
import {Observable} from 'rxjs';
import {Item} from '../../model/item.model';
import {take} from 'rxjs/operators';
import {MatDialog, MatSnackBar} from '@angular/material';
import {ConfirmationDialogComponent} from '../confirmation-dialog/confirmation-dialog.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-todos',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {

  items:Observable<Item[]>;
  editItems:boolean=false;
  sortBy:string="date";

  constructor(private itemService:ItemService,
              private dialog:MatDialog,
              private snackBar:MatSnackBar,
              private router:Router) { }

  ngOnInit() {
    this.loadItems();
  }

  loadItems(){
    this.items=this.itemService.getItems();
  }

  toggleDone(item: Item) {
    this.itemService.toggleDone(item).pipe(take(1)).subscribe(i=>{
      item=i;
    });
  }

  delete(item: Item) {
    const dialogRef=this.dialog.open(ConfirmationDialogComponent,{
      width:"350px",
      data:`Das ToDo "${item.title}" wirklich löschen?`
    });
    dialogRef.afterClosed().subscribe(result=>{
      if (result){
        this.itemService.deleteItem(item._id).pipe(take(1)).subscribe(()=>
            this.loadItems(),
          err=> this.snackBar.open("Es ist ein Fehler aufgetreten","Schließen",{duration:10000,panelClass:['warn-snackbar']})
        )
      }
    })

  }
}
