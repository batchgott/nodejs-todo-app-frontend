import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {Observable} from 'rxjs';
import {User} from '../../model/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm=new FormGroup({
    username:new FormControl('',[Validators.required]),
    password:new FormControl('',[Validators.required])
  });

  constructor(private snackBar:MatSnackBar,
              private router:Router,
              private userService:UserService) { }

  ngOnInit() {
  }

  onLogin() {
    let currentUser: Observable<User>;
    currentUser = this.userService.login(this.loginForm.get("username").value, this.loginForm.get("password").value);
    currentUser.subscribe((res: User) => {
      this.router.navigate(['']);
    },errorMessage=>{
      this.snackBar.open(errorMessage,"Schließen",{duration:10000,panelClass:['warn-snackbar']});
    });
  }
}
