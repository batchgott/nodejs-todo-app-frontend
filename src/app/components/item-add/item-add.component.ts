import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {ItemService} from '../../services/item.service';
import {Router} from '@angular/router';
import {take} from 'rxjs/operators';
import {Item} from '../../model/item.model';

@Component({
  selector: 'app-item-add',
  templateUrl: './item-add.component.html',
  styleUrls: ['./item-add.component.scss']
})
export class ItemAddComponent implements OnInit {

  addItemForm=new FormGroup({
    title:new FormControl('',[Validators.required]),
    description:new FormControl('',[Validators.required])
  });

  constructor(private snackBar:MatSnackBar,
              private itemService:ItemService,
              private router:Router) { }

  ngOnInit() {
  }

  addItem() {
    this.itemService.createItem(
      this.addItemForm.get("title").value,
      this.addItemForm.get('description').value
    ).pipe(take(1)).subscribe((item:Item)=>{
      this.snackBar.open(`Das Todo ${item.title} wurde erstellt`,null,{duration:3000});
      this.router.navigate(['/todos']);
    },err=>{
      this.snackBar.open("Es ist ein Fehler aufgetreten","Schließen",{duration:10000,panelClass:['warn-snackbar']});
    })
  }
}
