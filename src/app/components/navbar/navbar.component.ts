import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../model/user.model';
import {Subscription} from 'rxjs';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit,OnDestroy {

  isAuthenticated:boolean=false;
  currentUser:User;
  private userSub:Subscription;

  constructor(private userService:UserService,
              private router:Router) { }

  ngOnInit() {
    this.userSub=this.userService.user.subscribe(user=>{
      this.isAuthenticated=!user;
      this.currentUser=user;
    });
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }

  logout() {
    this.userService.logout();
  }

}
