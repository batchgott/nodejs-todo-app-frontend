import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from './app.component';
import {ItemsComponent} from './components/items/items.component';
import {LoginComponent} from './components/login/login.component';
import {AuthGuard} from './shared/auth-guard';
import {ItemAddComponent} from './components/item-add/item-add.component';
import {ItemEditComponent} from './components/item-edit/item-edit.component';


const routes: Routes = [
  {path:'',redirectTo:'todos',pathMatch:"full"},
  {path:'todos',canActivate:[AuthGuard],children:[
      {path:'',component:ItemsComponent},
      {path: "add",component: ItemAddComponent},
      {path: ":itemid",component:ItemEditComponent}
    ]},
  {path:"login",component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
