import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './components/navbar/navbar.component';
import {
    MatButtonModule,
    MatCardModule, MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule, MatListModule, MatSelectModule, MatSlideToggleModule,
    MatSnackBarModule,
    MatToolbarModule, MatTooltipModule
} from '@angular/material';
import { LoginComponent } from './components/login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ItemsComponent } from './components/items/items.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthInterceptorSevice} from './shared/auth-interceptor.service';
import { ItemAddComponent } from './components/item-add/item-add.component';
import {ConfirmationDialogComponent} from './components/confirmation-dialog/confirmation-dialog.component';
import { ItemEditComponent } from './components/item-edit/item-edit.component';
import { ItemSorterPipe } from './components/items/item-sorter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    ItemsComponent,
    ItemAddComponent,
    ConfirmationDialogComponent,
    ItemEditComponent,
    ItemSorterPipe
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        ReactiveFormsModule,
        MatCardModule,
        MatFormFieldModule,
        MatSnackBarModule,
        MatInputModule,
        HttpClientModule,
        MatListModule,
        MatTooltipModule,
        MatSlideToggleModule,
        FormsModule,
        MatDialogModule,
        MatSelectModule
    ],
  providers: [{provide:HTTP_INTERCEPTORS,useClass:AuthInterceptorSevice,multi:true}],
  bootstrap: [AppComponent],
  entryComponents:[ConfirmationDialogComponent]
})
export class AppModule { }
