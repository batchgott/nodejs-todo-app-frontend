import { Injectable } from '@angular/core';
import {BehaviorSubject, throwError} from 'rxjs';
import {User} from '../model/user.model';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public user:BehaviorSubject<User>=new BehaviorSubject<User>(null);
  private expirationTimer:any;

  constructor(private http:HttpClient,
              private router:Router) { }


  public login(username:string,password:string){
    return this.http.post<User>(environment.apiURL+"login",{
      "username":username,
      "password":password
    }).pipe(catchError(this.handleError),tap(resData=>{
      const user:User=resData;
      this.user.next(user);
      localStorage.setItem('userData',JSON.stringify(user));
      this.autoLogout(user.expirationDate);
    }));
  }

  private autoLogout(expirationDate:Date){
    this.expirationTimer=setTimeout(()=>this.logout(),new Date(expirationDate).getTime()-(new Date()).getTime());
  }

  public autoLogin(){
    const user:User=JSON.parse(localStorage.getItem('userData'));
    if (!user)return;

    if (!user.expirationDate||new Date()>user.expirationDate)return;
    this.user.next(user);
    this.autoLogout(user.expirationDate);
  }

  public logout(){
    this.user.next(null);
    localStorage.removeItem("userData");
    this.router.navigate(['login']);
    if (this.expirationTimer)clearTimeout(this.expirationTimer);
    this.expirationTimer=null;
  }

  private handleError(error:HttpErrorResponse){
    let errorMessage="Es ist ein Fehler aufgetreten";
    if (!error.error)return throwError(errorMessage);
    switch (error.error)
    {
      case "Username does not exist":
        errorMessage="Benutzer existiert nicht";break;
      case "Invalid password":
        errorMessage="Falsches Passwort";break;
      default:
        errorMessage="Es ist ein Fehler aufgetreten";break;
    }
    return throwError(errorMessage);
  }
}
