import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Item} from '../model/item.model';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(private http:HttpClient) { }

  public createItem(title:string,description:string){
    return this.http.post(environment.apiURL+"items/",{
      title:title,
      description:description
    })
  }

  public updateItem(item: Item){
    return this.http.put(environment.apiURL+"items/"+item._id,item);
  }

  public deleteItem(id){
    return this.http.delete(environment.apiURL+"items/"+id);
  }

  public toggleDone(item:Item):Observable<Item>{
    return this.http.patch<Item>(environment.apiURL+"items/"+item._id,{});
  }

  public getItemById(id):Observable<Item>{
    return this.http.get<Item>(environment.apiURL+"items/"+id);
  }

  public getItems(sortBy?:string):Observable<Item[]>{
    if (sortBy)
      return this.http.get<Item[]>(environment.apiURL+`items?sortBy=${sortBy}`);
    return this.http.get<Item[]>(environment.apiURL+"items");
  }
}
