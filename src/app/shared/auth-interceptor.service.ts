import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {exhaust, exhaustMap, take} from 'rxjs/operators';
import {UserService} from '../services/user.service';

@Injectable()
export class AuthInterceptorSevice implements HttpInterceptor{

  constructor(private userService:UserService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.userService.user.pipe(
      take(1),
      exhaustMap(user=>{
        if (!user)
          return next.handle(req);
        const modifiedRequest=req.clone({headers:new HttpHeaders().append("x-app-token",user.token)});
        return next.handle(modifiedRequest)
      })
    );

  }

}
